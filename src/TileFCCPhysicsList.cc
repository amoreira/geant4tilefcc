#include "TileFCCPhysicsList.hh"

//redacay01 example
#include "G4UnitsTable.hh"
#include "G4ParticleTypes.hh"
#include "G4IonConstructor.hh"
#include "G4PhysicsListHelper.hh"
#include "G4Radioactivation.hh"
#include "G4SystemOfUnits.hh"
#include "G4NuclideTable.hh"
#include "G4LossTableManager.hh"
#include "G4UAtomicDeexcitation.hh"
#include "G4NuclearLevelData.hh"
#include "G4DeexPrecoParameters.hh"
#include "G4NuclideTable.hh"

//B3 example
#include "G4DecayPhysics.hh"
#include "G4EmStandardPhysics.hh"
#include "G4RadioactiveDecayPhysics.hh"

// Optical physics
#include "FTFP_BERT.hh"
#include "G4OpticalPhysics.hh"
#include "G4EmStandardPhysics_option4.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TileFCCPhysicsList::TileFCCPhysicsList()
: G4VModularPhysicsList(){
  SetVerboseLevel(1);

  //redacay01 example
  //add new units for radioActive decays
  //
  const G4double minute = 60*second;
  const G4double hour   = 60*minute;
  const G4double day    = 24*hour;
  const G4double year   = 365*day;
  new G4UnitDefinition("minute", "min", "Time", minute);
  new G4UnitDefinition("hour",   "h",   "Time", hour);
  new G4UnitDefinition("day",    "d",   "Time", day);
  new G4UnitDefinition("year",   "y",   "Time", year);

  // mandatory for G4NuclideTable
  //
  G4NuclideTable::GetInstance()->SetThresholdOfHalfLife(0.1*picosecond);
  G4NuclideTable::GetInstance()->SetLevelTolerance(1.0*eV);

  //RegisterPhysics(new FTFP_BERT());   
  RegisterPhysics(new G4EmStandardPhysics_option4());

  // Optical physics   
  G4OpticalPhysics *opticalPhysics = new G4OpticalPhysics();   
  opticalPhysics->SetWLSTimeProfile("delta");   
  opticalPhysics->SetScintillationYieldFactor(1.0);   
  opticalPhysics->SetScintillationExcitationRatio(0.0);   
  //opticalPhysics->SetMaxNumPhotonsPerStep(100);   
  //opticalPhysics->SetMaxBetaChangePerStep(10.0);   
  //opticalPhysics->SetTrackSecondariesFirst(kCerenkov,true);   
  opticalPhysics->SetTrackSecondariesFirst(kScintillation,true);     

  RegisterPhysics(opticalPhysics);

  //B3 example
  /*
  // Default physics
  RegisterPhysics(new G4DecayPhysics());

  // EM physics
  RegisterPhysics(new G4EmStandardPhysics());

  // Radioactive decay
  RegisterPhysics(new G4RadioactiveDecayPhysics());
  */
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TileFCCPhysicsList::~TileFCCPhysicsList()
{
  //delete fEmPhysicsList;
}

void TileFCCPhysicsList::ConstructParticle()
{
  // pseudo-particles
  G4Geantino::GeantinoDefinition();

  // gamma
  G4Gamma::GammaDefinition();

  // optical photon
  G4OpticalPhoton::OpticalPhotonDefinition();

  // leptons
  G4Electron::ElectronDefinition();
  G4Positron::PositronDefinition();

  G4MuonPlus::MuonPlusDefinition();
  G4MuonMinus::MuonMinusDefinition();

  G4NeutrinoE::NeutrinoEDefinition();
  G4AntiNeutrinoE::AntiNeutrinoEDefinition();

  // baryons
  G4Proton::ProtonDefinition();
  G4AntiProton::AntiProtonDefinition();
  G4Neutron::NeutronDefinition();
  G4AntiNeutron::AntiNeutronDefinition();

  // mesons                                     
  G4PionPlus::PionPlusDefinition();
  G4PionMinus::PionMinusDefinition();
  G4KaonPlus::KaonPlusDefinition();
  G4KaonMinus::KaonMinusDefinition();

  // ions
  G4IonConstructor iConstructor;
  iConstructor.ConstructParticle();
}

void TileFCCPhysicsList::ConstructProcess()
{
  AddTransportation();
  
  G4VPhysicsConstructor *EmPhysList = new G4EmStandardPhysics_option4();
  EmPhysList->ConstructProcess();
  
  G4VPhysicsConstructor *OpPhysList = new G4OpticalPhysics();
  OpPhysList->ConstructProcess();  

  //AddStepMax();

  // AddTransportation();

  G4Radioactivation* radioactiveDecay = new G4Radioactivation();

  G4bool ARMflag = false;
  radioactiveDecay->SetARM(ARMflag);        //Atomic Rearangement

  // register radioactiveDecay
  //
  G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();
  ph->RegisterProcess(radioactiveDecay, G4GenericIon::GenericIon());

  //printout
  //
  G4cout << "\n  Set atomic relaxation mode " << ARMflag << G4endl;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TileFCCPhysicsList::SetCuts()
{
  //G4VUserPhysicsList::SetCuts();
  SetCutsWithDefault();
}
