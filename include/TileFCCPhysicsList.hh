#ifndef TileFCCPhysicsList_h
#define TileFCCPhysicsList_h 1

#include "G4VModularPhysicsList.hh"
#include "globals.hh"

class G4VPhysicsConstructor;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class TileFCCPhysicsList: public G4VModularPhysicsList
{
  public:
    TileFCCPhysicsList();
   ~TileFCCPhysicsList();

  protected:
    // Construct particle and physics
    virtual void ConstructParticle();
    virtual void ConstructProcess();
    virtual void SetCuts();
  
  private:
  //G4VModularPhysicsList *fEmPhysicsList;


};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
